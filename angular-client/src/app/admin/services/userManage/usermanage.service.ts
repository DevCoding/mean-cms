import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AuthService } from '../authService/auth.service';
import { environment } from '../../../../environments/environment';
import 'rxjs/add/operator/map';
@Injectable()
export class UsermanageService {

  constructor(private http:Http, private auth : AuthService) { }
  getUserlist(){
    let options = this.auth.createAuthenticationHeaders();
    return this.http.get(environment.domain + 'user/list', options).map(res => res.json());
  }
  createUser(user){
    console.log(user);
    let options = this.auth.createAuthenticationHeaders();
    return this.http.post(environment.domain + 'user',user,options).map(res => res.json());
  }
  updateUser(user){
    console.log(user);
    let options = this.auth.createAuthenticationHeaders();
    return this.http.put(environment.domain + 'user',user,options).map(res => res.json());
  }
  deleteUser(user){
    console.log(user);
    let options = this.auth.createAuthenticationHeaders();
    return this.http.delete(environment.domain + 'user/' + user._id,options).map(res => res.json());
  }
}
