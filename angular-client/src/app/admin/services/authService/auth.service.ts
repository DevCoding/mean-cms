import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
@Injectable()
export class AuthService {


  authtoken;
  user;
  options;
  constructor(
    private http: Http,
    private rotuer : Router
  ) {

  }
  createAuthenticationHeaders(){
    this.loadToken();
    console.log("authtoken : " + this.authtoken);
    this.options = new RequestOptions({
      headers : new Headers({
        'Authorization' : "JWT " + this.authtoken
      })
    });

    return this.options;
  }
  loadToken(){
    const token = localStorage.getItem('token');
    this.user = JSON.parse(localStorage.getItem('user'));
    this.authtoken = token;
  }
  logout(){
    let options = this.createAuthenticationHeaders();
    return this.http.get(environment.domain + 'logout',options).map(res => res.json()).subscribe(result => {
      alert(result.message);
      this.authtoken = null;
      this.user= null;
      localStorage.clear();
      this.rotuer.navigate(['/signin-up/sign-in']);
    });


  }
  loggedIn(){
    return tokenNotExpired();
  }
  storeUserData(token,user){
    this.user = user;
    this.authtoken = token;
    localStorage.setItem('token',token);
    localStorage.setItem('user',JSON.stringify(user));
  }

  login(user){
    return this.http.post(environment.domain + 'login', user).map(res => res.json());
  }
  registerUser(user){
    return this.http.post(environment.domain + 'register',user).map(res => res.json());
  }

}
