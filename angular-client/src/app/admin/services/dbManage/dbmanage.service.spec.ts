import { TestBed, inject } from '@angular/core/testing';

import { DbmanageService } from './dbmanage.service';

describe('DbmanageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DbmanageService]
    });
  });

  it('should be created', inject([DbmanageService], (service: DbmanageService) => {
    expect(service).toBeTruthy();
  }));
});
