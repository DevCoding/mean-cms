import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AuthService } from '../authService/auth.service';
import { environment } from '../../../../environments/environment';
import 'rxjs/add/operator/map';
@Injectable()
export class DbmanageService {

  constructor(private http:Http, private auth : AuthService) { }

  getdevice_groupList(){
    let options = this.auth.createAuthenticationHeaders();
    return this.http.get(environment.domain + 'device/group', options).map(res => res.json());
  }

  addDevice(device){
    let options = this.auth.createAuthenticationHeaders();
    return this.http.post(environment.domain + 'device', device, options).map(res => res.json());
  }

  getDevices(){
    let options = this.auth.createAuthenticationHeaders();
    return this.http.get(environment.domain + 'device',options).map(res => res.json());
  }

  updateDevice(device){
    let options = this.auth.createAuthenticationHeaders();
    console.log("service is running");
    console.log(device);
    return this.http.put(environment.domain + 'device',device,options).map(
      res => res.json()
    );
  }

  getDevicesbygroupId(id){
    let options = this.auth.createAuthenticationHeaders();
    console.log(id);
    return this.http.get(environment.domain + 'device/group/' + id, options ).map(res => res.json());
  }

  deleteDevicebyId(id){
    let options = this.auth.createAuthenticationHeaders();
    console.log(id);
    return this.http.delete(environment.domain + 'device/' + id, options).map(res => res.json());
  }

  updateDeviceGroup(devicegroup){
    let options = this.auth.createAuthenticationHeaders();
    console.log(devicegroup);
    return this.http.put(environment.domain + 'device/group', devicegroup, options).map(res => res.json());
  }
  createDeviceGroup(devicegroup){
    let options = this.auth.createAuthenticationHeaders();
    return this.http.post(environment.domain + 'device/group', devicegroup, options).map(res => res.json());
  }

  updateDevicelistGroupname(groupname, devicegroup){
    let options = this.auth.createAuthenticationHeaders();
    return this.http.put(environment.domain + 'device/groupname/' + groupname, devicegroup, options).map(res => res.json());
  }
}
