import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevicegroupeditComponent } from './devicegroupedit.component';

describe('DevicegroupeditComponent', () => {
  let component: DevicegroupeditComponent;
  let fixture: ComponentFixture<DevicegroupeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevicegroupeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevicegroupeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
