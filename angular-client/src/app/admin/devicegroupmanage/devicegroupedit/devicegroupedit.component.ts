import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../layouts/shared-service';
import { Router } from '@angular/router';
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import { DbmanageService} from '../../services/dbManage/dbmanage.service';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { EditdeviceComponent } from '../../devicemanage/editdevice/editdevice.component';
@Component({
  selector: 'app-devicegroupedit',
  templateUrl: './devicegroupedit.component.html',
  styleUrls: ['./devicegroupedit.component.scss']
})
export class DevicegroupeditComponent implements OnInit {
  pageTitle = "DeviceGroup -> GroupEdit"

  deviceGroup;
  devices = [];
  devices_temp = [];
  constructor(
    private shared_Service : SharedService,
    private router : Router,
    private localstorage_Service : LocalstorageService,
    private dbservice : DbmanageService,
    public dialog: MdDialog
  ) {
      shared_Service.emitChange(this.pageTitle);
      this.getDevicebyId();

  }
  getDevicebyId(){
    this.deviceGroup = Object.assign({},this.localstorage_Service.buf_deviceGroup);
    if (this.localstorage_Service.buf_deviceGroup == undefined){
      this.router.navigate(["/admin/devicegroupmanage"]);
    } else {
      var __this = this;
      this.dbservice.getDevicesbygroupId(this.deviceGroup.groupname).subscribe(devices => {
        __this.devices = devices.object;
        __this.devices_temp = [].concat(__this.devices);
        var index = 0;
        __this.devices.forEach(device => {
          device.no = index = index + 1;
        });
      },
      error => {
        alert("You are not authorized");
      });
    }
  }
  updateFilter(event){
    const val = event.target.value.toLowerCase();
    var devices_temp = this.devices_temp.filter(devicegroup => {
      return devicegroup.devicename.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.devices = devices_temp;
  }
  ngOnInit() {

  }
  onBack(){
    this.router.navigate(["/admin/devicegroupmanage"]);
  }
  remove(device){
    this.dbservice.deleteDevicebyId(device._id).subscribe(result => {
      if (result.success == true){
        alert("Device was deleted successfully");
        this.getDevicebyId();
      }
    },
    error => {
      alert("You are not authorized");
    });
  }
  onSave(){
    var __this = this;
    __this.dbservice.updateDeviceGroup(__this.deviceGroup).subscribe(result => {
      if (result.success == true){

        var orig_groupname = __this.localstorage_Service.buf_deviceGroup.groupname;

        __this.deviceGroup = result.object[0];

        __this.localstorage_Service.buf_deviceGroup = __this.deviceGroup;

        __this.dbservice.updateDevicelistGroupname(orig_groupname, __this.deviceGroup).subscribe(result => {
          if (result.success == true){
            __this.devices = result.object;
            alert("Updated");
            __this.getDevicebyId();
          }
          else {
            alert(result.message);
          }
        },
        error => {
          alert("You are not authorized");
        });
      }
      else{
        alert(result.message);
        __this.deviceGroup = __this.localstorage_Service.buf_deviceGroup;
      }
    },
    error => {
      alert("You are not authorized");
    });
  }
  onAdd(){
    let dialogRef = this.dialog.open(EditdeviceComponent, {
      panelClass: 'col-md-3'
    });

     dialogRef.componentInstance.formValue = {
        devicename: '',
        address: '',
        latitude: 0,
        longitude: 0,
        onservice: new Date(),
        model: '',
        version: '',
        dimlevel: 1,
        switch: false,
        activepower: 0,
        min: 0,
        max: 0,
        reset: false,
        devicegroup: ''
     };
     dialogRef.componentInstance.dialogTitle = "Device Add";
    dialogRef.afterClosed().subscribe(result => {
      if (result == "cancel"){

      }
      else
      {
        var controls = dialogRef.componentInstance.form.controls;

        var newDevice = {
          devicename : controls["devicename"].value,
          address   : controls["address"].value,
          latitude  : Number(controls["latitude"].value),
          longitude : Number(controls["longitude"].value),
          onservice   : controls["onservice"].value,//Date
          model     : controls["model"].value,
          version   : controls["version"].value,
          dimlevel : Number(controls["dimlevel"].value),
          reset : controls["reset"].value,
          devicegroup : controls["devicegroup"].value
          //switch,activepower,min,max are missing
        };

        this.dbservice.addDevice(newDevice).subscribe(result => {
          if (result.success == true){
            this.getDevicebyId();
          }
        },
        error => {
          alert("You are not authorized");
        });


      }
    });
  }
}
