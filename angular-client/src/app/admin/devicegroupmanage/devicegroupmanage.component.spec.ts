import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevicegroupmanageComponent } from './devicegroupmanage.component';

describe('DevicegroupmanageComponent', () => {
  let component: DevicegroupmanageComponent;
  let fixture: ComponentFixture<DevicegroupmanageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevicegroupmanageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevicegroupmanageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
