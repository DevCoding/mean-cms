import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedService } from '../../layouts/shared-service';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { DbmanageService } from '../services/dbManage/dbmanage.service';
import { Router} from '@angular/router';
import { LocalstorageService } from '../services/localstorage/localstorage.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DevicegroupaddComponent } from './devicegroupadd/devicegroupadd.component';
@Component({
  selector: 'app-devicegroupmanage',
  templateUrl: './devicegroupmanage.component.html',
  styleUrls: ['./devicegroupmanage.component.scss']
})
export class DevicegroupmanageComponent implements OnInit {
  pageTitle : string = "DeviceGroup";
  lat: number = 25.2048;
  lng: number = 55.2708;
  devices = [];
  loadingIndicator = true;
  circleArray = [];
  mapObject : any;
  selected: any[] = [];
  deviceGroupList = [];
  deviceGroupList_temp = [];
  @ViewChild(DatatableComponent) table : DatatableComponent;
  constructor(
    private shared_Service : SharedService,
    public dialog: MdDialog,
    private dbservice : DbmanageService,
    private router : Router,
    private localstorageservice : LocalstorageService
  ) {
    shared_Service.emitChange(this.pageTitle);

  }
  onAdd(){
    let dialogRef = this.dialog.open(DevicegroupaddComponent, {
      //panelClass: 'col-md-3'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == "cancel"){

      }
      else {
        var groupname = dialogRef.componentInstance.form.controls["devicegroupname"].value;
        this.dbservice.createDeviceGroup({
          groupname: groupname,
          deviceuid: "",
          profile: ""
        }).subscribe(result => {
          this.getDeviceandGroup();
        },
        error => {
          alert("You are not authorized");
        });
      }
    });
  }
  updateFilter(event){
    const val = event.target.value.toLowerCase();
    var deviceGroupList_temp = this.deviceGroupList_temp.filter(devicegroup => {
      return devicegroup.groupname.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.deviceGroupList = deviceGroupList_temp;
    this.table.offset = 0;
  }
  getDeviceandGroup(){
    var __this = this;
    __this.circleArray = [];
    __this.dbservice.getDevices().subscribe(deviceList => {
      __this.devices = deviceList.object;

      var index = 0;
      __this.devices.forEach(device => {
        device.no = index = index + 1;
        // this.circleArray.push(L.circle([device.latitude, device.longitude], {
        //     color: '#dc143c',
        //     fillColor: '#dc143c',
        //     fillOpacity: 0.2,
        //     radius: 300
        // }).bindPopup(device.devicegroup));
        var myIcon = L.icon({
          iconUrl: 'assets/img/mark-pin.png',
          iconSize: [20, 45],
          iconAnchor: [8, 45],
          popupAnchor: [-3, -35],
          shadowUrl: 'assets/img/marker-shadow.png',
          shadowSize: [45, 47],
          shadowAnchor: [9, 47]
        });
        __this.circleArray.push(L.marker([device.latitude, device.longitude],{icon: myIcon}).bindPopup(device.devicegroup));
      });
      __this.circleArray.forEach(circle => {
        circle.addTo(__this.mapObject);
      });
      __this.dbservice.getdevice_groupList().subscribe( devicegroups => {
        __this.deviceGroupList = devicegroups.object;
        __this.deviceGroupList_temp = [].concat(__this.deviceGroupList);
        var index = 0;
        __this.deviceGroupList.forEach(devicegroup => {
          devicegroup.no = index = index + 1;
          var count = 0;
          __this.devices.forEach(device => {
            if (devicegroup.groupname == device.devicegroup){
              count = count + 1;
            }
          });
          devicegroup.deviceCount = count;
        });
      },
      error => {
        alert("You are not authorized");
      });
    },
    error => {
      alert("You are not authorized");
    });
  }
  ngOnInit() {
    this.mapObject = L.map('map').setView([this.lat, this.lng], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoibmV4dC1pdGVtIiwiYSI6ImNqMDFlYWRqeTAyNzEyd3FuNjQxdmVvMjgifQ.Ff8pEWrzeJ3uipr78e69uw', {
      maxZoom: 18,
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
      '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
      'Imagery © <a href="http://mapbox.com">Mapbox</a>',
      id: 'mapbox.streets'
    }).addTo(this.mapObject);

    this.getDeviceandGroup();


  }
  onEdit(number){
    var deviceGroup = this.deviceGroupList[number];
    this.localstorageservice.buf_deviceGroup = deviceGroup;

    this.router.navigate(["/admin/devicegroupedit"]);
  }
}
