import { ViewChild, Component, OnInit, ElementRef } from '@angular/core';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { DbmanageService } from '../../services/dbManage/dbmanage.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
@Component({
  selector: 'app-devicegroupadd',
  templateUrl: './devicegroupadd.component.html',
  styleUrls: ['./devicegroupadd.component.scss']
})
export class DevicegroupaddComponent implements OnInit {
  form: FormGroup;
  constructor(
    public dialogRef: MdDialogRef<DevicegroupaddComponent>,
    private dbservice : DbmanageService,
    private fb: FormBuilder,
    private localstorage : LocalstorageService
  ) { 
    
  }

  ngOnInit() {
    this.form = this.fb.group({
      devicegroupname : [null, Validators.compose([Validators.required])]
    });
  }

}
