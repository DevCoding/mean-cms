import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevicegroupaddComponent } from './devicegroupadd.component';

describe('DevicegroupaddComponent', () => {
  let component: DevicegroupaddComponent;
  let fixture: ComponentFixture<DevicegroupaddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevicegroupaddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevicegroupaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
