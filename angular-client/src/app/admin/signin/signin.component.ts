import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { AuthService } from '../services/authService/auth.service';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  form: FormGroup;

  messsageClass;
  message = '';
  constructor(private fb: FormBuilder, private authService : AuthService, private router : Router) { }

  ngOnInit( ) {
    this.form = this.fb.group({
      email: [null, Validators.compose([Validators.required, CustomValidators.email])],
      password: [null, Validators.compose([Validators.required,CustomValidators.password])]
    });
  }
  onSignin(){
    const user = {
      email : this.form.get("email").value,
      password : this.form.get("password").value,
    };

    this.authService.login(user).subscribe(data => {
      if (!data.success){
        //this.messsageClass = "'danger'";
        //this.message = data.message;
        alert(data.message);
      } else{
        //this.messsageClass = "'success'";
        //this.message = data.message;
        alert(data.message);
        this.authService.storeUserData(data.token, data.object);
        setTimeout(() => {
          this.router.navigate(['/admin/dashboard']);

        },2000);
      }
    });
  }

}
