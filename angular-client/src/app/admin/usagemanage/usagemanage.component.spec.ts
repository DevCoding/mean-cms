import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsagemanageComponent } from './usagemanage.component';

describe('UsagemanageComponent', () => {
  let component: UsagemanageComponent;
  let fixture: ComponentFixture<UsagemanageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsagemanageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsagemanageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
