import { Component, OnInit, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MdDatepickerModule } from '@angular/material'; 
import { DbmanageService } from '../services/dbManage/dbmanage.service';
@Component({
  selector: 'app-usagemanage',
  templateUrl: './usagemanage.component.html',
  styleUrls: ['./usagemanage.component.scss']
})

export class UsagemanageComponent implements OnInit {
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    responsiveAnimationDuration: 500,
    barPercentage: 0.2
  };
  public barChartLabels: string[] = [
    '2012',
    '2013',
    '2014',
    '2015',
    '2016',
    '2017'
  ];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  public barChartData: any[] = [
    {
      data: [59, 80, 81, 56, 55, 40],
      label: 'Device count',
      borderWidth: 2
    },
    {
      data: [48, 40, 19, 86, 27, 90],
      label: 'Productive Capacity',
      borderWidth: 2
    }
  ];
  public barChartColors: any[] = [
    {
      backgroundColor: 'rgba(226,50,55,0.2)',
      borderColor: '#e23237'
    },
    {
      backgroundColor: 'rgba(97,218,251,0.2)',
      borderColor: '#61dafb'
    }
  ];

  filteredRoles: any;
  roles = [ "Weekly", "Monthly", "Yearly"];

  maxDate : Date = new Date();
  toDate : Date;
  fromDate : Date;
  
  toDateStr : string; lowLimit : Date;
  fromDateStr : string; maxLimit : Date;
  roleCtrl: FormControl;
  constructor(private rd : Renderer2,private dbservice : DbmanageService) { }

  ngOnInit() {
    this.roleCtrl = new FormControl();
    this.roleCtrl.setValue("Weekly");
    this.filteredRoles = this.roleCtrl.valueChanges
    .startWith(null)
    .map(name => this.filterRoles(name));

    this.toDate = new Date(this.maxDate.getTime());

    this.fromDate = new Date(this.toDate.getTime());
    this.fromDate.setTime(this.fromDate.getTime() - 24*60*60*7*1000);
    
    this.lowLimit = this.fromDate;
    this.updateChartContent();
    this.updateChartTitle();

    this.dbservice.getdevice_groupList().subscribe(result => {
      if (result.success == true){
        this.barChartLabels = [];
        result.object.forEach(devicegroup => {
          this.barChartLabels.push(devicegroup.groupname);
        });
      }
    },
    error => {
      alert("You are not authorized");
    });
  }
  filterRoles(val: string) {
    return val ? this.roles.filter((s) => new RegExp(val, 'gi').test(s)) : this.roles;
  }
  fromDatechange($event){
    console.log($event);
    this.fromDate = $event;

    this.lowLimit = $event;
    this.updateChartContent();
    this.updateChartTitle();
  }
  toDatechange($event){
    console.log($event);
    this.toDate = $event;

    this.updateChartContent();
    this.updateChartTitle();
  }
  updateChartTitle(){
    if (this.roleCtrl.value == "Weekly"){
      this.fromDateStr = (this.lowLimit.getMonth() + 1) + "/" + this.lowLimit.getDate() + "/" + this.lowLimit.getFullYear();

      this.maxLimit = new Date(this.lowLimit.getTime() + 24*60*60*7*1000);
      if (this.maxLimit.getTime() > this.toDate.getTime()){
        this.maxLimit = new Date(this.toDate.getTime());
      }
      this.toDateStr = (this.maxLimit.getMonth() + 1) + "/" + this.maxLimit.getDate() + "/" + this.maxLimit.getFullYear();
    }
    if (this.roleCtrl.value == "Monthly"){
      this.fromDateStr = (this.lowLimit.getMonth() + 1) + "/" + this.lowLimit.getDate() + "/" + this.lowLimit.getFullYear();
      
      this.maxLimit = new Date(this.lowLimit.getTime() + 24*60*60*30*1000);
      if (this.maxLimit.getTime() > this.toDate.getTime()){
        this.maxLimit = new Date(this.toDate.getTime());
      }
      this.toDateStr = (this.maxLimit.getMonth() + 1) + "/" + this.maxLimit.getDate() + "/" + this.maxLimit.getFullYear();
    }
    if (this.roleCtrl.value == "Yearly"){
      this.fromDateStr = (this.lowLimit.getMonth() + 1) + "/" + this.lowLimit.getDate() + "/" + this.lowLimit.getFullYear();
      
      this.maxLimit = new Date(this.lowLimit.getTime() + 24*60*60*365*1000);
      if (this.maxLimit.getTime() > this.toDate.getTime()){
        this.maxLimit = new Date(this.toDate.getTime());
      }
      this.toDateStr = (this.maxLimit.getMonth() + 1) + "/" + this.maxLimit.getDate() + "/" + this.maxLimit.getFullYear();
    }
  }
  updateChartContent(){

  }

  onBack(){
    if (this.lowLimit.getTime() == this.fromDate.getTime()){

    } else {
      if (this.roleCtrl.value == "Weekly"){
        this.maxLimit = new Date(this.lowLimit.getTime());
        this.lowLimit.setTime(this.lowLimit.getTime() - 24*60*60*7*1000);
        if (this.lowLimit.getTime() < this.fromDate.getTime()){
          this.lowLimit = new Date(this.fromDate.getTime());
        }
      }
      if (this.roleCtrl.value == "Monthly"){
        this.maxLimit = new Date(this.lowLimit.getTime());
        this.lowLimit.setTime(this.lowLimit.getTime() - 24*60*60*30*1000);
        if (this.lowLimit.getTime() < this.fromDate.getTime()){
          this.lowLimit = new Date(this.fromDate.getTime());
        }
      }
      if (this.roleCtrl.value == "Yearly"){
        this.maxLimit = new Date(this.lowLimit.getTime());
        this.lowLimit.setTime(this.lowLimit.getTime() - 24*60*60*365*1000);
        if (this.lowLimit.getTime() < this.fromDate.getTime()){
          this.lowLimit = new Date(this.fromDate.getTime());
        }
      } 
      this.updateChartTitle();
      this.updateChartContent();
    }
  }
  onForward(){
    if (this.maxLimit.getTime() == this.toDate.getTime()){

    } else {
      if (this.roleCtrl.value == "Weekly"){
        this.lowLimit = new Date(this.maxLimit.getTime());
        this.maxLimit.setTime(this.maxLimit.getTime() + 24*60*60*7*1000);
        if (this.maxLimit.getTime() > this.toDate.getTime()){
          this.maxLimit = new Date(this.toDate.getTime());
        }
      }
      if (this.roleCtrl.value == "Monthly"){
        this.lowLimit = new Date(this.maxLimit.getTime());
        this.maxLimit.setTime(this.maxLimit.getTime() + 24*60*60*30*1000);
        if (this.maxLimit.getTime() > this.toDate.getTime()){
          this.maxLimit = new Date(this.toDate.getTime());
        }
      }
      if (this.roleCtrl.value == "Yearly"){
        this.lowLimit = new Date(this.maxLimit.getTime());
        this.maxLimit.setTime(this.maxLimit.getTime() + 24*60*60*365*1000);
        if (this.maxLimit.getTime() > this.toDate.getTime()){
          this.maxLimit = new Date(this.toDate.getTime());
        }
      }
      this.updateChartTitle();
      this.updateChartContent();
    }
  }
}
