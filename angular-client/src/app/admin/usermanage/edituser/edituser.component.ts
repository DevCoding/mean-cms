import { ViewChild, Component, OnInit, ElementRef } from '@angular/core';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { DbmanageService } from '../../services/dbManage/dbmanage.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
const password = new FormControl('', Validators.required);
const confirmpassword = new FormControl('', CustomValidators.equalTo(password));
@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.scss']
})
export class EdituserComponent implements OnInit {
  form: FormGroup;
  formValue : any;
  dialogTitle = "Add User";
  filteredRoles: any;
  checkboxHide : boolean = true;
  roles = [ "level1", "level2"];
  constructor(
    public dialogRef: MdDialogRef<EdituserComponent>,
    private dbservice : DbmanageService,
    private fb: FormBuilder,
    private localstorage : LocalstorageService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      firstname: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(10)])],
      lastname: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(10)])],
      email: [null, Validators.compose([Validators.required, CustomValidators.email])],
      password: password,
      confirmpassword: confirmpassword,
      organization : [null, Validators.compose([Validators.required])],
      role : [null, Validators.compose([Validators.required])],
      device_groups : [null]
    });
    if (this.dialogTitle == "Edit User"){
      this.form.controls["firstname"].setValue(this.formValue["firstname"]);
      this.form.controls["lastname"].setValue(this.formValue["lastname"]);
      this.form.controls["email"].setValue(this.formValue["email"]);
      this.form.controls["password"].setValue(this.formValue["password"]);
      this.form.controls["confirmpassword"].setValue(this.formValue["password"]);
      this.form.controls["organization"].setValue(this.formValue["organization"]);
      this.form.controls["role"].setValue(this.formValue["role"]);
      this.form.controls["device_groups"].setValue([]);
      this.checkboxHide = false;

      this.form.controls["password"].disable();
      this.form.controls["confirmpassword"].disable();
      this.checkboxHide = false;
    }
    else {
      this.form.controls["device_groups"].setValue([]);
      this.form.controls["password"].enable();
      this.form.controls["confirmpassword"].enable();
      this.form.controls["password"].setValue("");
      this.form.controls["confirmpassword"].setValue("");
      this.checkboxHide = true;
    }
    this.filteredRoles = this.form.controls["role"].valueChanges
      .startWith(null)
      .map(name => this.filterRoles(name));
  }
  filterRoles(val: string) {
    return val ? this.roles.filter((s) => new RegExp(val, 'gi').test(s)) : this.roles;
  }
  oncheckstatus($event){
    // console.log($event);
    // this.checked = $event.checked;

    if ($event.checked == true){
      this.form.controls["password"].enable();
      this.form.controls["confirmpassword"].enable();
      this.form.controls["password"].setValue("");
      this.form.controls["confirmpassword"].setValue("");
    } else{
      this.form.controls["password"].disable();
      this.form.controls["confirmpassword"].disable();
      this.form.controls["password"].setValue(this.formValue["password"]);
      this.form.controls["confirmpassword"].setValue(this.formValue["password"]);
    }
  }
}
