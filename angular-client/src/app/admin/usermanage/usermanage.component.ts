import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedService } from '../../layouts/shared-service';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { EdituserComponent } from './edituser/edituser.component';
import { DbmanageService } from '../services/dbManage/dbmanage.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FormControl } from '@angular/forms';
import { UsermanageService } from '../services/userManage/usermanage.service'; 
@Component({
  selector: 'app-usermanage',
  templateUrl: './usermanage.component.html',
  styleUrls: ['./usermanage.component.scss']
})
export class UsermanageComponent implements OnInit {
  pageTitle = "User Management"
  users = [];
  users_temp = [];
  loadingIndicator = true;
  constructor(
    private shared_Service : SharedService,
    public dialog: MdDialog,
    private dbservice : DbmanageService,
    private userservice : UsermanageService 
  ) {
    shared_Service.emitChange(this.pageTitle);
    
  }
  updateFilter(event){
    const val = event.target.value.toLowerCase();
    var users_temp = this.users_temp.filter(user => {
      return user.email.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.users = users_temp;
  }
  getUserlist(){
    this.userservice.getUserlist().subscribe(result => {
      if (result.success == true){
        this.users = result.object;
        this.users_temp = [].concat(this.users);
        console.log(this.users);
        var index = 0;
        this.users.forEach( user => {
          user.no = index = index + 1;
        });
      }
      else {
        alert("Userlist fetching error");
      }
    },
    error => {
      alert("You are not authorized");
    });
  }
  ngOnInit() {
    this.getUserlist();
  }
  onEdit(number){
    let dialogRef = this.dialog.open(EdituserComponent, {
      panelClass: 'col-md-3'
    });
    dialogRef.componentInstance.formValue = this.users[number];
    dialogRef.componentInstance.dialogTitle = "Edit User";
    dialogRef.afterClosed().subscribe(result => {
      if (result == "cancel"){

      }
      else {
        var value = dialogRef.componentInstance.form.controls;
        var user = {
          _id : this.users[number]._id,
          firstname : value["firstname"].value,
          lastname : value["lastname"].value,
          email : value["email"].value,
          organization : value["organization"].value,
          role : value["role"].value,
          password : value["password"].value ,
          device_groups : value["device_groups"].value
        }
        this.userservice.updateUser(user).subscribe(result => {
          if (result.success == true){
            this.getUserlist();
          } else{
            alert(result.message);
          }
        },
        error => {
          alert("You are not authorized");
        });
      }
    });
  }
  onRemove(number){
    var user = this.users[number];

    this.userservice.deleteUser(user).subscribe(result => {
      if (result.success == true){
        this.getUserlist();
      } else{
        alert(result.message);
      }
    },
    error => {
      alert("You are not authorized");
    });
  }
  onAdd(){
    // var device = this.devices[number];
    let dialogRef = this.dialog.open(EdituserComponent, {
      panelClass: 'col-md-3'
    });
    dialogRef.componentInstance.dialogTitle = "Add User";
    dialogRef.afterClosed().subscribe(result => {
      if (result == "cancel"){

      }
      else {
        var value = dialogRef.componentInstance.form.controls;
        var user = {
          firstname : value["firstname"].value,
          lastname : value["lastname"].value,
          email : value["email"].value,
          organization : value["organization"].value,
          role : value["role"].value,
          password : value["password"].value ,
          device_groups : value["device_groups"].value
        }
        this.userservice.createUser(user).subscribe(result => {
          if (result.success == true){
            this.getUserlist();
          } else{
            alert(result.message);
          }
        },
        error => {
          alert("You are not authorized");
        });
      }
    });
  }
}
