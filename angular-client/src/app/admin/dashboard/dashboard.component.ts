import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../layouts/shared-service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  pageTitle : string = "Dashboard";
  constructor(private shared_Service : SharedService) {
    shared_Service.emitChange(this.pageTitle);
  }

  ngOnInit() {
  }

}
