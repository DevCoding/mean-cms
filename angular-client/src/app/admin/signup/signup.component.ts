import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { AuthService } from '../services/authService/auth.service';
import 'rxjs/add/operator/startWith';
const password = new FormControl('', Validators.required);
const confirmPassword = new FormControl('', CustomValidators.equalTo(password));
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  public form: FormGroup;
  messsageClass;
  filteredRoles: any;

  roles = [ "Superadministrator","Administrator", "Manager", "Maintenancer"];
  constructor( private fb: FormBuilder, private authService : AuthService, private router : Router) {
    
  }
  filterRoles(val: string) {
    return val ? this.roles.filter((s) => new RegExp(val, 'gi').test(s)) : this.roles;
  }

  ngOnInit() {
    this.form = this.fb.group({
      fname: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(10)])],
      lname: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(10)])],
      email: [null, Validators.compose([Validators.required, CustomValidators.email])],
      password: password,
      confirmPassword: confirmPassword,
      organization : [null, Validators.compose([Validators.required])],
      role : [null, Validators.compose([Validators.required])]
    });
    this.filteredRoles = this.form.controls["role"].valueChanges
      .startWith(null)
      .map(name => this.filterRoles(name));
    
  }
  onSignup(){
    const user = {
      email : this.form.get("email").value,
      firstname : this.form.get("fname").value,
      lastname :  this.form.get("lname").value,
      password : this.form.get("password").value,
      organization : this.form.get("organization").value,
      role : this.form.get("role").value.toLowerCase()
    };
    this.authService.registerUser(user).subscribe(data => {
      if (!data.success){
        //this.messsageClass = "'danger'";
        //this.message = data.message;
        alert(data.message);
      } else{
        //this.messsageClass = "'success'";
        //this.message = data.message;
        alert(data.message);
        this.authService.storeUserData(data.token, data.object);
        setTimeout(() => {
           this.router.navigate(['/admin/dashboard']);
          // if (data.object.role == "user"){
          //   //this.router.navigate(['#']);
          // } else {
          //   if (data.object.role == "admin"){
          //       //this.router.navigate(['#']);
          //   }

          // }

        },2000);
      }
    });
  }
}
