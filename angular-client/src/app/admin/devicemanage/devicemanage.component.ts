import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedService } from '../../layouts/shared-service';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { EditdeviceComponent } from './editdevice/editdevice.component';
import { DbmanageService } from '../services/dbManage/dbmanage.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-devicemanage',
  templateUrl: './devicemanage.component.html',
  styleUrls: ['./devicemanage.component.scss']
})

export class DevicemanageComponent implements OnInit {
  pageTitle : string = "Device Management";
  lat: number = 25.2048;
  lng: number = 55.2708;
  devices : any;
  devices_temp = [];
  loadingIndicator = true;
  circleArray = [];
  mapObject : any;


  @ViewChild(DatatableComponent) table : DatatableComponent;
  constructor(private shared_Service : SharedService, public dialog: MdDialog, private dbservice : DbmanageService ) {
    shared_Service.emitChange(this.pageTitle);

  }

  updateFilter(event){
    const val = event.target.value.toLowerCase();
    var devices_temp = this.devices_temp.filter(device => {
      return device.devicename.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.devices = devices_temp;
    this.table.offset = 0;
  }
  getDevice(){
    var __this = this;
    __this.dbservice.getDevices().subscribe(deviceList => {
      if (deviceList.success == true){
        __this.devices = deviceList.object;
        __this.devices_temp = [].concat(__this.devices);
        var index = 0;
        __this.devices.forEach(device => {
          device.no = index = index + 1;
          // this.circleArray.push(L.circle([device.latitude, device.longitude], {
          //     color: '#dc143c',
          //     fillColor: '#dc143c',
          //     fillOpacity: 0.2,
          //     radius: 300
          // }).bindPopup(device.devicegroup));
  
          var myIcon = L.icon({
            iconUrl: 'assets/img/mark-pin.png',
            iconSize: [20, 45],
            iconAnchor: [8, 45],
            popupAnchor: [-3, -35],
            shadowUrl: 'assets/img/marker-shadow.png',
            shadowSize: [45, 47],
            shadowAnchor: [9, 47]
          });
          __this.circleArray.push(L.marker([device.latitude, device.longitude],{icon: myIcon}).bindPopup(device.devicegroup));
        });
        __this.circleArray.forEach(circle => {
          circle.addTo(__this.mapObject);
        });
      }
        

    },
    error => {
      alert("You are not authorized");
    }
    );
  }
  ngOnInit() {
    this.mapObject = L.map('map').setView([this.lat, this.lng], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoibmV4dC1pdGVtIiwiYSI6ImNqMDFlYWRqeTAyNzEyd3FuNjQxdmVvMjgifQ.Ff8pEWrzeJ3uipr78e69uw', {
      maxZoom: 18,
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
      '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
      'Imagery © <a href="http://mapbox.com">Mapbox</a>',
      id: 'mapbox.streets'
    }).addTo(this.mapObject);
    this.getDevice();

  }
  onEdit(number){
    var device = this.devices[number];
    let dialogRef = this.dialog.open(EditdeviceComponent, {
      panelClass: 'col-md-3'
    });

    dialogRef.componentInstance.formValue = device;
    dialogRef.afterClosed().subscribe(result => {
      if (result == "cancel"){

      }
      else {
        var controls = dialogRef.componentInstance.form.controls;

        var updatedDevice = {
          _id : device._id,
          devicename : controls["devicename"].value,
          address   : controls["address"].value,
          latitude  : Number(controls["latitude"].value),
          longitude : Number(controls["longitude"].value),
          onservice   : controls["onservice"].value,//Date
          model     : controls["model"].value,
          version   : controls["version"].value,
          dimlevel : Number(controls["dimlevel"].value),
          reset : controls["reset"].value,
          devicegroup : controls["devicegroup"].value
        };
        console.log("updated");
        console.log(updatedDevice);
        this.dbservice.updateDevice(updatedDevice).subscribe(result => {
          console.log(result);
          this.circleArray.forEach( circle => {
            this.mapObject.removeLayer(circle);
          });
          this.circleArray = [];
          this.getDevice();
        },
        error => {
          alert("You are not authorized");
        });
      }
    });
  }
}
