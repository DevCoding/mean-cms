import { ViewChild, Component, OnInit, ElementRef } from '@angular/core';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { DbmanageService } from '../../services/dbManage/dbmanage.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
@Component({
  selector: 'app-editdevice',
  templateUrl: './editdevice.component.html',
  styleUrls: ['./editdevice.component.scss']
})

export class EditdeviceComponent implements OnInit {

  deviceGroupList = [];
  form: FormGroup;
  formValue : any;
  dialogTitle = "Device Edit";
  constructor(
    public dialogRef: MdDialogRef<EditdeviceComponent>,
    private dbservice : DbmanageService,
    private fb: FormBuilder,
    private localstorage : LocalstorageService
  ) { }

  ngOnInit() {


    this.form = this.fb.group({
      devicename : [null, Validators.compose([Validators.required])],
      address :[null, Validators.compose([Validators.required])],
      latitude: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(90), CustomValidators.number])],
      longitude: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(90), CustomValidators.number])],
      model : [null, Validators.compose([Validators.required])],
      version : [null, Validators.compose([Validators.required])],
      onservice: [null, Validators.compose([Validators.required, CustomValidators.date])],
      devicegroup: [null, Validators.compose([Validators.required])],
      reset: [null, Validators.compose([Validators.required])],
      dimlevel: [null, Validators.compose([Validators.required])],
    });
    if (this.dialogTitle == "Device Edit"){
      this.form.controls['model'].disable();
      this.form.controls['version'].disable();

      this.dbservice.getdevice_groupList().subscribe( devicegroups => {
        this.deviceGroupList = devicegroups.object;
      },
      error => {
        alert("You are not authorized");
      });
    }
    else {
      this.deviceGroupList.push(this.localstorage.buf_deviceGroup);
    }
    
    this.form.controls["devicename"].setValue(this.formValue["devicename"]);
    this.form.controls["address"].setValue(this.formValue["address"]);
    this.form.controls["latitude"].setValue(this.formValue["latitude"]);
    this.form.controls["longitude"].setValue(this.formValue["longitude"]);
    this.form.controls["model"].setValue(this.formValue["model"]);
    this.form.controls["version"].setValue(this.formValue["version"]);
    this.form.controls["onservice"].setValue(this.formValue["onservice"]);
    this.form.controls["devicegroup"].setValue(this.formValue["devicegroup"]);
    this.form.controls["dimlevel"].setValue(this.formValue["dimlevel"].toString());
    this.form.controls["reset"].setValue(this.formValue["reset"]);



  }
  onchangetoggle($event){
    this.form.controls["reset"].setValue($event.checked);
    console.log(this.form.controls["reset"].value);
  }
  onchangeslider($event){
    this.form.controls["dimlevel"].setValue($event.value.toString());
    console.log(this.form.controls["dimlevel"].value);
  }

}
