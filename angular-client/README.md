# Angular UI

This angular project is using angular-4 bootstrap admin panel(https://wrapbootstrap.com/theme/ng-app-angular-4-bootstrap-4-admin-WB0761624).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

We can run this project using this command.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Path

Admin source codes are at `/src/app`