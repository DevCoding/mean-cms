const LocalStrategy = require('passport-local').Strategy;
const User = require('../database/user');
const config = require('./config');

var passport = require("passport");
var passportJWT = require("passport-jwt");

var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;

var auth = require('../helper/auth');
/**
 * Expose
 */
var jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
jwtOptions.secretOrKey = config.secret;

module.exports = new JwtStrategy(jwtOptions, function(jwt_payload, done) {
  console.log('payload received', jwt_payload);
  // usually this would be a database call:
  User.findById(jwt_payload._id, (err, user) => {
    if (err)  done(err)
    if (!user) {
     done(null, false, { message: 'Unknown user' });
    }
    // if (!user.comparePassword(password)) {
    //     return done(null, false, { message: 'Invalid password' });
    // }
     done(null, user);  
  });
});
/*module.exports = new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  function(email, password, done) {
    // var options = {
    //   criteria: { email: email },
    //   select: config.secret
    // };
    // User.load(options, function (err, user) {
    //   if (err) return done(err)
    //   if (!user) {
    //     return done(null, false, { message: 'Unknown user' });
    //   }
    //   if (!user.authenticate(password)) {
    //     return done(null, false, { message: 'Invalid password' });
    //   }
    //   return done(null, user);
    // });
    User.find({email : email.toLowerCase()},(err,user) => {
      if (err) return done(err)
      if (!user) {
        return done(null, false, { message: 'Unknown user' });
      }
      if (!user.comparePassword(password)) {
        return done(null, false, { message: 'Invalid password' });
      }
      return done(null, user);
    });
  }
);*/