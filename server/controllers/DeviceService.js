'use strict';
var DeviceGroup = require('../database/devicegroup');
var Device = require('../database/device');
var ObjectId = require('mongoose').Types.ObjectId;
// var passport = require('passport');
//switch,activepower,min,max are missing

exports.getdevicegrouplist = function(args, res, next){
  DeviceGroup.find({}, (err, devicegroups) => {

    res.json({
      success : true,
      message : "Device Group lists",
      object : devicegroups
    })
  });
}
exports.createdevicegroup = function(args, res, next) {
  /**
   * parameters expected in the args:
  * devicegroup (DeviceGroup)
  **/
  var value = args.devicegroup.value;

  var deviceGroup = DeviceGroup({
    groupname : value.groupname,
    deviceuid : value.deviceuid,
    profile: value.profile
  });

  deviceGroup.save( err => {
    if (err){
      if (err.code == 11000){
        res.json({success : false, message : "Groupname already exists"});
      } else {
        if (err.errors){
          res.json({success : false, message : err});
        }
        else {
          res.json({success : false, message : "Couldn't register groupname. Error : ",err});
        }
      }
    }
    else{
      DeviceGroup.findOne({groupname : deviceGroup.groupname},(err,val) => {
        res.json({
          success : true,
          message : "Group name registered",
          object : val});
      });
    }
  });
}

exports.deviceCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  **/

    var value = args.device.value;
    console.log("Comming");
    console.log(device);
    var device = Device(value);
    device.save( (err,device) => {
      if (err){
        res.json({success:false, message:"Err",err})
      } else{
        res.json({
          success : true,
          message : "Device created",
          object : device});
      }
    });
}

exports.getdeviceList = function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
    Device.find({},(err, devices) => {
      if (err)
      {
      }
      else {
        res.json({
          success : true,
          message : "AllDeviceList",
          object : devices
        })
      }

    });

}

exports.getdevicebyID = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  console.log(args);

}

exports.getdevicelistbyGroupID = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    console.log(args);
    var id = args.id.value;
    Device.find({devicegroup : id}, (err, devices) => {
      if (err){
        console.log(err);
      } else {
        console.log(devices);
        res.json({
          success : true,
          message : "Devicelist by groupid",
          object : devices
        })

      }
    });

}
exports.updatedevicegroup = function(args, res, next){
  console.log("update comming");
  var devicegroup = args.devicegroup.value;
  DeviceGroup.update({_id : devicegroup._id}, devicegroup, (err, status) => {
    if (err){
      console.log(err);
      res.json({
        success : false,
        message : "Devicegroup update failed"
      })
    } else {


      DeviceGroup.find({_id : devicegroup._id}, (err1, devicegroup1) => {
        if (err1){

        }
        else {

          // Device.find({devicegroup : devicegroup1.groupname}, (err, devices) => {
          //   if (err){
          //     console.log(err);
          //   } else {
          //     devices.forEach(device => {
          //       device.devicegroup = devicegroup1.groupname;
          //       console.log(device);
          //       device.save((err,result) => {});
          //     });
          //   }
          //
          // });
          res.json({
            success : true,
            message : "Devicegroup updated",
            object : devicegroup1
          })
        }
      });
    }
  });
}
exports.updatedevicebyID = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * device (Device)
  **/
  var updateDevice = args.device.value;
  console.log("Comming in");
  Device.update({_id : updateDevice._id}, updateDevice, (err, status) => {
    if (err){
      console.log(err);
    } else{
      
      res.json({
        success : true,
        message : "Device updated",
        object : status
      })
    }
  });

}
exports.deleteDevice = function(args, res, next){

  var id = args.id.value;
  Device.findByIdAndRemove(id, (err,device) => {
    res.json({
      success : true,
      message : "Deletion was done successfully",
      object : device
    });
  })
}
exports.devices_groupnameupdate = function(args, res, next){
  console.log(args);

  var groupname = args.groupname.value;
  var devicegroup = args.devicegroup.value;
  Device.find({devicegroup : groupname}, (err, devices) => {
    if (err){
      console.log(err);
      if (err.code == 11000){
        res.json({
          success : false,
          message : "Wrong edition: You are trying to change the group name which is existing"
        });
      } else{
        res.json({
          success : false,
          message : "Editing failed"
        });
      }
    } else {
      if (devices.length > 0){
        devices.forEach(device => {
          device.devicegroup = devicegroup.groupname;
          device.save();
          
        });
        res.json({
          success : true,
          message : "updated Devicegroup for devices"
        }); 
      } else {
        res.json({success : true, message : "updated Devicegroup with no device"});
      }
    }

  });
}
