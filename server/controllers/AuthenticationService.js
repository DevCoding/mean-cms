'use strict';
var auth = require('../helper/auth');
var User = require('../database/user');
var passport = require('passport');
exports.login = function(args, res, next) {

  var email = args.authObject.value.email;
  var password = args.authObject.value.password;

  if (email != "" && password != ""){
    
    User.findOne({email : email.toLowerCase()},(err,user) => {
      if (err){
        res.json({success : false, message : err});
      } else{
        if (!user){
          res.json({success : false, message : "Email not found"});
        } else{
          const validPassword = user.comparePassword(password);
          if (!validPassword){
            res.json({success : false, message : "Password invalid"});
          } else{
            
            const token = auth.generateToken(user._id, user.role);
            var result = {
              success : true,
              message : "Login Success",
              object: user,
              token : token
            }
            res.setHeader('Content-Type', 'application/json');
            res.json(result);

            
          }
        }
      }
    });
  }
}

exports.register = function(args, res, next) {
  /**
   * parameters expected in the args:
  * user (User)
  **/

  var value = args.user.value;
  var user = User({
        email : value.email.toLowerCase(),
        firstname : value.firstname,
        lastname : value.lastname,
        password : value.password,
        role : value.role,
        organization : value.organization,
        device_groups : value.device_groups
      });
      user.save(err => {
        if (err){
          if (err.code == 11000){
            res.json({success : false, message : "User name and email already exists"});
          } else {
            if (err.errors){
              if (err.errors.email){
                res.json({success : false, mesage : err.errors.email.message});
              } else{
                if (err.errors.username){
                  res.json({success : false, mesage : err.errors.username.message});
                } else{
                  if (err.errors.password){
                    res.json({success : false, mesage : err.errors.password.message});
                  } else {
                    res.json({success : false, message : err});
                  }
              }
            }
          } else {
            res.json({success : false, message : "Couldn't register user. Error : ",err})
          }
        }
      } else {

        User.findOne({email : user.email.toLowerCase()},(err,val) => {
          const token = auth.generateToken(val._id, value.role);
          res.json({
            success : true,
            message : "User registered",
            object : val,
            token : token});
        });
        
      };
    });
}
exports.logout = function(args, res, next){
  console.log(args);

  res.req.logout();
  
}