'use strict';

var url = require('url');


var User = require('./UserService');


module.exports.deleteuserbyID = function deleteuserbyID (req, res, next) {
  User.deleteuserbyID(req.swagger.params, res, next);
};

module.exports.getuserbyID = function getuserbyID (req, res, next) {
  User.getuserbyID(req.swagger.params, res, next);
};

module.exports.updateuserbyID = function updateuserbyID (req, res, next) {
  User.updateuserbyID(req.swagger.params, res, next);
};

module.exports.userCreate = function userCreate (req, res, next) {
  User.userCreate(req.swagger.params, res, next);
};
module.exports.getuserlist = function getuserlist (req, res, next){
  User.getuserlist(req.swagger.params, res, next);
}