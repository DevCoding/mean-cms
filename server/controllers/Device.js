'use strict';

var url = require('url');


var Device = require('./DeviceService');

module.exports.createdevicegroup = function createdevicegroup (req, res, next) {
  Device.createdevicegroup(req.swagger.params, res, next);
};

module.exports.deviceCreate = function deviceCreate (req, res, next) {
  console.log("Device Create");
  Device.deviceCreate(req.swagger.params, res, next);
};

module.exports.getdeviceList = function getdeviceList (req, res, next) {
  Device.getdeviceList(req.swagger.params, res, next);
};

module.exports.getdevicebyID = function getdevicebyID (req, res, next) {
  Device.getdevicebyID(req.swagger.params, res, next);
};

module.exports.getdevicelistbyGroupID = function getdevicelistbyGroupID (req, res, next) {
  Device.getdevicelistbyGroupID(req.swagger.params, res, next);
};

module.exports.updatedevicebyID = function updatedevicebyID (req, res, next) {
  Device.updatedevicebyID(req.swagger.params, res, next);
};
module.exports.getdevicegrouplist = function getdevicegrouplist (req, res, next) {
  Device.getdevicegrouplist(req.swagger.params, res, next);
};
module.exports.deleteDevice = function deleteDevice (req, res, next){
  Device.deleteDevice(req.swagger.params, res, next);
}
module.exports.updatedevicegroup = function updatedevicegroup (req, res, next){
  Device.updatedevicegroup(req.swagger.params, res, next);
}
module.exports.devices_groupnameupdate = function devices_groupnameupdate(req, res, next){
  Device.devices_groupnameupdate(req.swagger.params, res, next);
}
