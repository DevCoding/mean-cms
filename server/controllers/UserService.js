'use strict';
var User = require('../database/user');
exports.deleteuserbyID = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  console.log(args);
  var id = args.id.value;
  
  User.findByIdAndRemove(id, (err,user) => {
    if (err){
      res.json({
        success : false,
        message : "Deletion failed"
      });
    } else{
      res.json({
        success : true,
        message : "Deletion was done successfully",
        object : user
      });
    }
    
  })
}

exports.getuserbyID = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "success" : true,
  "message" : "aeiou",
  "object" : {
    "firstname" : "aeiou",
    "password" : "aeiou",
    "role" : "aeiou",
    "device_groups" : [ "aeiou" ],
    "id" : "aeiou",
    "email" : "aeiou",
    "lastname" : "aeiou"
  }
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.updateuserbyID = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * user (User)
  **/
  var value = args.user.value;
  console.log(value);
  User.findById(value._id, (err,user) => {
    if (err){
      res.json({
        success : false,
        message : "User not found"
      })
      console.log(err);
    }else {
      
      user.firstname = value.firstname;
      user.lastname = value.lastname;
      user.email = value.email;
      user.organization = value.organization;
      user.role = value.role;
      user.password = value.password;
      user.device_groups = value.device_groups;
      user.save((err1, saved_user) => {
        if (err1){
          console.log(err1);
          res.json({
            success : false,
            message : "User update failed"
          })  
        } else {
          
          res.json({
            success : true,
            message : "User updated",
            object : saved_user
          })
          console.log("Updated");
        }
      })
    }
  });
}
exports.getuserlist = function(args, res, next){
  User.find({}, (err, users) => {
    if (err){
      res.json({
        success : true,
        message : "Cannot get list"
      })
    } else{
      res.json({
        success : true,
        message : "UserList",
        object : users
      })
    }
  });
}
exports.userCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * user (GeneralResponse)
  **/
  var value = args.user.value;

  var user = new User({
    email : value.email.toLowerCase(),
    firstname : value.firstname,
    lastname : value.lastname,
    password : value.password,
    role : value.role,
    organization : value.organization,
    device_groups : value.device_groups
  });
  user.save((err, user) => {
    if (err){
      if (err.code == 11000){
        res.json({success : false, message : "User name or email already exists"});
      } else {
        if (err.errors){
          if (err.errors.email){
            res.json({success : false, mesage : err.errors.email.message});
          } else{
            if (err.errors.username){
              res.json({success : false, mesage : err.errors.username.message});
            } else{
              if (err.errors.password){
                res.json({success : false, mesage : err.errors.password.message});
              } else {
                res.json({success : false, message : err});
              }
            }
          }
        } else {
          res.json({success : false, message : "Couldn't register user. Error : ",err})
        }
      }
    } else{
      res.json({
        success : true,
        message : "User Created",
        object : user
      })
    }
  });

  
}

