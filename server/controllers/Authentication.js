'use strict';

var url = require('url');

var Authentication = require('./AuthenticationService');
module.exports.login = function login(req, res, next) {
  Authentication.login(req.swagger.params, res, next);
};

module.exports.register = function register(req, res, next) {
  Authentication.register(req.swagger.params, res, next);
};
module.exports.logout = function logout(req, res, next){
  req.logout();
  res.json({success : false, message : "Logout"});
  //Authentication.logout(req.swagger.params, res, next);
}