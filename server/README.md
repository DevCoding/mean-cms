## Node JS Server

Node JS is used for backend and working on http://localhost:8080.

Run `node index` at 'MEAN-STACK/server' path to run this server codes.

`/api` : Swagger script folder which is containing yaml file.

`/config` : Initialize settings for backend. (Mongodb host, db name, secret key, server host)

`/controllers` : Node JS server controller codes.

`/helper` : Authorization checking code.

## MongoDB database


Mongodb is the database for this server and its host can be found in `/config/config.js`.

Currently, it is `mongodb://localhost:27017/meanstackdb`.

To run mongodb, mongodb should be installed locally.

And simply `mongod` to run mongodb host locally.

Mongodb schemas are at `/database` path.

## How to access API

JWT and Passport are used for authorization.
 
ServerAPI should be called with `Authorization` header and `JWT ******`.

