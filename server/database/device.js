const mongoose = require('mongoose');
const Schema = mongoose.Schema;
      ObjectId = Schema.ObjectId;
mongoose.Promise = global.Promise;

const deviceSchema = new Schema({
  devicename : String,
  address   : String,
  latitude  : Number,
  longitude : Number,
  onservice   : String,//Date
  model     : String,
  version   : String,
  dimlevel : Number,
  switch : Boolean,
  activepower : Number,
  min : Number,
  max : Number,
  reset : Boolean,
  devicegroup : String
});

module.exports = mongoose.model('Device', deviceSchema);
