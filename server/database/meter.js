const mongoose = require('mongoose');
const Schema = mongoose.Schema;
      ObjectId = Schema.ObjectId;
mongoose.Promise = global.Promise;

const meterSchema = new Schema({
  device_uid : { type : ObjectId, required : true},
  energy : Number,
  from : String, // Date
  to : String   // Date
});

module.exports = mongoose.model('Meter', meterSchema);
