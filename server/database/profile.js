const mongoose = require('mongoose');
const Schema = mongoose.Schema;
      ObjectId = Schema.ObjectId;
mongoose.Promise = global.Promise;

const profileSchema = new Schema({
  description : {type : String, required : true},
  actions : [{name : String, start : String, end : String}]//start,end : Date
});

module.exports = mongoose.model('Profile', profileSchema);
