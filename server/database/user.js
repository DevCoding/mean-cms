const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;
        ObjectId = Schema.ObjectId;
const bcrypt = require('bcrypt-nodejs');
const DeviceGroup = require('./devicegroup');


const userSchema = new Schema({
  email : {type : String, required : true, unique : true, lowercase : true},
  firstname : {type : String, required : true},
  lastname : {type : String, required : true},
  organization : {type : String, required:true},
  password : {type : String, required : true},
  role : {type : String, required : true},
  device_groups : [String]
});

userSchema.pre('save', function(next){
  if (!this.isModified('password'))
    return next();

  bcrypt.hash(this.password,null,null, (err,hash) => {
    if (err) return next(err);
    this.password = hash;
    next();
  });
});
userSchema.pre('update', function(next){
  if (!this.isModified('password'))
    return next();

  bcrypt.hash(this.password,null,null, (err,hash) => {
    if (err) return next(err);
    this.password = hash;
    next();
  });
});
userSchema.methods.comparePassword = function(password){
  return bcrypt.compareSync(password, this.password);
};
module.exports = mongoose.model('User', userSchema);
