const mongoose = require('mongoose');
const Schema = mongoose.Schema;
      ObjectId = Schema.ObjectId;
mongoose.Promise = global.Promise;

const devicegroupSchema = new Schema({
  groupname : {type : String, required : true, unique : true},
  deviceuid : {type : String, default: null},
  profile: {type : String, default:null}
});

module.exports = mongoose.model('DeviceGroup', devicegroupSchema);
