"use strict";

var jwt = require("jsonwebtoken");
var config = require('../config/config');
var passport = require('passport');
//Here we setup the security checks for the endpoints
//that need it (in our case, only /protected). This
//function will be called every time a request to a protected
//endpoint is received

exports.verifyToken = function(req, def, token, callback) {

  //these are the scopes/roles defined for the current endpoint

  //validate the 'Authorization' header. it should have the following format:
  //'Bearer tokenString'
  passport.authenticate('jwt', function (err, user,info) {
    if (err) {
      callback(new Error('Error in passport authenticate'));
    } else if (!user) {
      callback(new Error('Failed to authenticate oAuth token'));
    } else {
      var currentScopes = req.swagger.operation["x-security-scopes"];
      if (token && token.indexOf("JWT ") == 0) {
        var tokenString = token.split(" ")[1];
        jwt.verify(tokenString,config.secret, function(
          verificationError,
          decodedToken
        ){
          if ( verificationError == null && Array.isArray(currentScopes) && decodedToken && decodedToken.role){
            var roleMatch = currentScopes.indexOf(decodedToken.role) != -1;
            if (roleMatch){
              req.user = user;
              callback();
            } else{
              callback(new Error("Failed to authenticate role"));
            }
          } 
        });
      }

    }

  })(req, null, callback);
};

exports.generateToken = function(userId,role){
  var token = jwt.sign({
    _id : userId,
    role : role 
  }, config.secret, {expiresIn : '24h'});
  console.log(token);
  return token;
};

