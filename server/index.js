'use strict';
var express = require("express");
var app = express();
var swaggerTools = require('swagger-tools');
var jsyaml = require('js-yaml');
var auth = require("./helper/auth");
var fs = require('fs');
var mongoose = require("./node_modules/mongoose");
var config = require("./config/config");
var YAML = require("yamljs");
const cors = require('cors');
var bodyParser = require('body-parser');
var passport = require('passport');
var authentication = require('./controllers/Authentication');
var device = require('./controllers/Device');
var user = require('./controllers/User');
// swaggerRouter configuration
var options = {
  swaggerUi: __dirname + '/api/swagger/swagger.json',
  controllers: __dirname + '/controllers',
  useStubs: process.env.NODE_ENV === 'development' ? true : false // Conditionally turn on stubs (mock mode)
};

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var spec = fs.readFileSync(__dirname + '/api/swagger/swagger.yaml', 'utf8');
var swaggerDoc = jsyaml.safeLoad(spec);

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {
  // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
  mongoose.Promise = global.Promise;
  mongoose.connect(config.uri, (err) =>{
    if (err){
      console.log("Couldn't connect ot database : ",err);
    }
    else{
      //console.log(config.secret);
      console.log("Connected to database : "+ config.db);
    }
  });
 
  app.use(cors({
    origin : 'http://localhost:4200'
  }));
  app.use(middleware.swaggerMetadata());
  app.use(middleware.swaggerUi());
  app.use(middleware.swaggerValidator());
  app.use(middleware.swaggerSecurity({
      //manage token function in the 'auth' module
      JWT: auth.verifyToken
    })
  );

  app.use(middleware.swaggerMetadata()); // needs to go after swaggerSecurity, otherwise using 'Authorization' header from swagger api_key doesn't authenticate properly
  app.use(passport.initialize());
  app.use(passport.session());
  require('./config/passport')(passport);

  app.get('/logout', passport.authenticate('jwt', {session : false}), authentication.logout);
  app.get('/device',passport.authenticate('jwt', { session: false }), device.getdeviceList);
  app.post('/device',passport.authenticate('jwt', { session: false }), device.deviceCreate);
  app.put('/device',passport.authenticate('jwt', { session: false }), device.updatedevicebyID);
  app.put('/device/groupname/:groupname',passport.authenticate('jwt', { session: false }), device.devices_groupnameupdate);
  app.put('/device/group/:id',passport.authenticate('jwt', { session: false }), device.getdevicelistbyGroupID);
  app.post('/device/group',passport.authenticate('jwt', { session: false }), device.createdevicegroup);
  app.get('/device/group',passport.authenticate('jwt', { session: false }), device.getdevicegrouplist);
  app.put('/device/group',passport.authenticate('jwt', { session: false }), device.updatedevicegroup);
  app.get('/device/:id',passport.authenticate('jwt', { session: false }), device.getdevicebyID);
  app.delete('/device/:id',passport.authenticate('jwt', { session: false }), device.deleteDevice);

  app.get('/user/list',passport.authenticate('jwt', { session: false }), user.getuserlist);
  app.get('/user/:id',passport.authenticate('jwt', { session: false }), user.getuserbyID);
  app.delete('/user/:id',passport.authenticate('jwt', { session: false }), user.deleteuserbyID);
  app.post('/user',passport.authenticate('jwt', { session: false }), user.userCreate);
  app.put('/user',passport.authenticate('jwt', { session: false }), user.updateuserbyID);

  app.use(middleware.swaggerRouter(options)); 
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  app.use(express.static(__dirname + "../meancms_angular/dist/"));
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + "../meancms_angular/dist/index.html"))
  });
  app.listen(8080, function() {
    console.log("Started server on port 8080");
  });

});

